<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $email
 * @property bool   $active
 */
class User extends Model
{
    use HasFactory;

    public static function getActive(int $id): ?self
    {
        return self::where('id', $id)
            ->where('active', true)
            ->firstOrFail();
    }

    public function addToGroup(int $groupId): void 
    {
        $this->groups()->attach($groupId);
        $this->active = true;
        $this->save();

    }

    public function deactivate(): void
    {
        $exist = GroupUser::where('user_id', $this->id)->find();
        if (!$exist) {
            $this->active = false;
            $this->save();
        }
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_user');
    }
}
