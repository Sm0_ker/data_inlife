<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property int    $expire_hours
 */
class Group extends Model
{
    use HasFactory;

    public function groups()
    {
        return $this->belongsToMany(User::class, 'group_user');
    }
}
