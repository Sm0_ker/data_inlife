<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Group;
use App\Mail\DetachEmail;
use App\Models\GroupUser;
use Illuminate\Support\Facades\Mail;

class UserGroupObserver
{
    /**
     * Handle the GroupUser "created" event.
     *
     * @param  \App\Models\GroupUser  $groupUser
     * @return void
     */
    public function created(GroupUser $groupUser)
    {
        $group = Group::find($groupUser->group_id);
        $groupUser->expire_at = Carbon::now()->addHours($group->expire_hours);
        $groupUser->save();
    }

    /**
     * Handle the GroupUser "updated" event.
     *
     * @param  \App\Models\GroupUser  $groupUser
     * @return void
     */
    public function updated(GroupUser $groupUser)
    {
        //
    }

    /**
     * Handle the GroupUser "deleted" event.
     *
     * @param  \App\Models\GroupUser  $groupUser
     * @return void
     */
    public function deleted(GroupUser $groupUser)
    {
        $user = User::find($groupUser->user_id);
        Mail::to($user->email)->send(new DetachEmail());
        $user->deactivate();
    }

    /**
     * Handle the GroupUser "restored" event.
     *
     * @param  \App\Models\GroupUser  $groupUser
     * @return void
     */
    public function restored(GroupUser $groupUser)
    {
        //
    }

    /**
     * Handle the GroupUser "force deleted" event.
     *
     * @param  \App\Models\GroupUser  $groupUser
     * @return void
     */
    public function forceDeleted(GroupUser $groupUser)
    {
        //
    }
}
