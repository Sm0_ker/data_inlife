<?php

namespace App\Providers;

use App\Models\GroupUser;
use App\Observers\UserGroupObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        GroupUser::observe(UserGroupObserver::class);
    }
}
