<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Group;
use Illuminate\Console\Command;

class UserMember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:member {user} {group}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user to group';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userId  = (int) $this->argument('user');
        $groupId = (int) $this->argument('group');

        $user = User::getActive($userId);
        $group = Group::find($groupId);

        if ($user === null) {
            return Command::INVALID;
        }

        if ($group === null) {
            return Command::INVALID;
        }

        $user->addToGroup($groupId);
        

        return Command::SUCCESS;
    }
}
