<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Console\Command;

class UserCheckExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:check_expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Detach all expired users';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        User::with('groups')
            ->wherePivot('expired_at', '<', Carbon::now())
            ->detach();

        return Command::SUCCESS;
    }
}
