<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Group::factory()->create([
            'name'          => 'Группа 1',
            'expire_hours'  => 1,
        ]);
        
        Group::factory()->create([
            'name'          => 'Группа 2',
            'expire_hours'  => 2,
        ]);

        User::factory()->create([
            'name'  => 'Иванов',
            'email' => 'info@datainlife.ru'
        ]);

        User::factory()->create([
            'name'  => 'Петров',
            'email' => 'job@datainlife.ru'
        ]);
    }
}
